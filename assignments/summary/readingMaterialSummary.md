# Basics of Industrial IoT

## IoT

- The Internet of things is a system of **interrelated computing devices, mechanical and digital machines** provided with unique identifiers and the ability to transfer data over a network without requiring human-to-human or human-to-computer interaction.
- The embedded technology in the objects helps them to interact with internal states or the external environment, which in turn affects the decisions taken.

   ![IOT](https://iotdunia.com/wp-content/uploads/2016/09/iot-architecture.jpg)

## IIoT

- **The Industrial Internet of Things (IIoT)** refers to interconnected sensors, instruments, and other devices networked together with computers' industrial applications, including manufacturing and energy management.
- This connectivity allows for data collection, exchange, and analysis, potentially facilitating improvements in productivity and efficiency as well as other economic benefits.
**Industrial settings** with respect to instrumentation and control of sensors and devices that engage **cloud technologies**.
 
## Industrial Revoution

- Industry 1.0 - Mechanization, Steam Power, Weaving loom
- Industry 2.0 - Mass production, Assembly lines, Electrical energy
- Industry 3.0 - Automation, Computers and Electronics,Information Technology **(Transition)**
- Industry 4.0 - Cyber Physical Systems, Internet of Things, Network **(Present)**


   ![Stages of IIoT](https://qph.fs.quoracdn.net/main-qimg-f13420fe3c6853609f40befb261777ec)</div>


### Industry 3.0

- Data is stored in databases and represented in excel sheets.
- Automation, Computers & Electronics.
- One of the major highlight of this revolution is the capablity of storing the acquried data from sensors in databases


#### Industry 3.0 Architecture

>
> **Sensors --> PLC -->SCADA & ERP**
>
- Sensors installed at various points in the Factory send data to PLC's which collect all the data and send it to SCADA and  ERP systems for storing the data.
- Data is stored in Excels and CSV's.

- The devices of this revoution had the following architecture 
    * Field devices
        * sensors
        * acutators
        * Motors
    
    * Control devices
        * Microcontrollers
        * Computer Numeric Controls

    * Stations
    * WorkCentres
        * Use softwares like scada,excel to store data 
    * Enterprises

    * The field bus connects the sensors and controllers.


   ![Architecture](https://www.elomatic.com/en/assets/images/utility-pages/expert_articles/industrial_automation_diagram%201.png)


#### Industry 3.0 Communication Protocols

All these protocols are optimized for sending data to a central server inside thefactory.

Few protocols are mentioned below-
 - ModBUS
 - CanOpen
 - ETHERCat


### Industry 4.0 

**Industry 4.0 is _Industry 3.0_ connected to Internet, which is called IoT.**  
- Connecting to Internet makes data communication faster, easier without data loss.
- Data Gathering brings more advancements like : 

  * Showing Dashboards
  * Remote Web SCADA
  * Remote control configuration of devices.
  * Predictive maintenance.
  * Real-time event processing.
  * Analytics with predictive models.
  * Automated device provisioning (Auto discovery).
  * Real-time alerts & alarms.  etc..  
  
- Here data is stored in **Cloud**, which decreases human efforts in data management. 

#### Indsutry 4.0 Architecture

- IoT Gateway acts as a bridge between PLCs and SCADA, Converts the data received from them and send to Cloud(translated output for cloud)
- Edge gets the data from controllers and convert it to a cloud understandable protocol.


   ![Architecture](https://image.slidesharecdn.com/opensourceforindustry4-171120165931/95/open-source-software-for-industry-40-6-638.jpg?cb=1511204195)
  


#### Industry 4.0 Communication Protocols

  Some _Protocols_ used to send data to _cloud_ for data analysis are- 
  - MQTT.org
  -   AMQP
  -   OPC UA 
  -   CoAP RFC 7252
  -   HTTP
  -   WebSocket 
  -   RESTful API etc. 
 
  
### Industry 3.0 Protocols,Industry 4.0 Protocols and its conversion


   ![Protocol conversion](https://gitlab.com/mpmayuripandey12/iotmodule1/-/raw/master/extras/conversion.png)

### Problems faced by Factory Owners for Industry 4.0 Upgradation and their solutions
***
#### Problems

- **Cost:** Factory owners don't want to switch into Industry 4.0, because it is _Expensive_.
- **Downtime:** Changing Hardware would result in downtime and nobody want to face such loss.
- **Reliability:** Investing in devices which are unproven and unreliable is a risk.

#### Solutions

- Its pretty solution is : "Getting data from the devices already present in the factory and sending it to cloud."
- So, there is no need to replace the original devices, causing lower risk factor.
- We have to **Change the _Industry 3.0 Protocols_ to _Industry 4.0 Protocols_.**
- However some Challenges faced in this Conversion are :
  * Expensive Hardware
  * Lack of documentation
  * Propritary PLC Protocols

#### How to Convert
***
- We have a library known as Shunaya Interface that helps get data from _Industry 3.0 devices_ and send to _Industry 4.0 cloud_.

   ![Conversion of 3.0 into 4.0](https://techinsight.com.vn/wp-content/uploads/2018/07/4-1.png)


#### Steps to make an IIoT Product
***
- Identify most popular Industry 3.0 devices.
- Study Protocols that these devices Communicate.
- Get data from the Industry 3.0 devices.
- Send the data to cloud for Industry 4.0.
  


#### What's Next
***
After sending data to cloud, Data Analysis is carried out with the help of several tools.

TSDB is preferred 
- It affixs a timestamp to the incoming fields.
- The fields dont have to be predefined like traditional SQL databases

Various TSDB providers are :
- INFLUX db
- Promethus
- Things Board
- Grafana

These data can be stored in IoT platforms like 
- AWS
- Azure
- Google Firebase

   ![iot platforms](https://devopedia.org/images/article/86/2438.1528650763.jpg)


Alerts or notifications can be recieved using services like
- Twilio
- Zapier
- Integromat
- IFTTT
